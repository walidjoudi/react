import React, { useState } from 'react';
import {Link, useLocation} from 'react-router-dom';
import {
  Nav,
  NavItem,
} from 'reactstrap';
import { Sidebarinfo } from './Sidebarinfo';

const Dashboard = () => {
  const [active, setActive] = useState("")

  const location = useLocation();

  const modifier = (url) => {
    const pathname = window.location.pathname
    if(url == pathname) setActive("active")
    else  setActive("")
  }
  
  return (
          <Nav vertical className="Sidebar">
            {Sidebarinfo.map((info, i) => {
              return (
                <NavItem onClick={() => modifier(info.path) } key={i} className={info.path == location.pathname ? active : ""}>
                  <Link to={info.path}>
                      {info.title}
                  </Link>
                </NavItem>
              )
            })}
          </Nav>
  );
}

export default Dashboard;