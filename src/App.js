import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Dashboard from './Dashboard';
import Calculate from './pages/tp1';
import Articles from './pages/tp2/Articles';
import Repos from './pages/tp3/Repos';
import AccountCircleIcon from '@material-ui/icons';
import Avatar from '@material-ui/core/Avatar';

function App() {
  return (
    <BrowserRouter>
      <div className="Userbar">
        <Avatar src="../_DSC3166.JPG" style={{ height: '70px', width: '70px' }}/>
        <div>
          <h5>JOUDI Walid</h5>
          <span>ISI5</span>
        </div>
      </div>
      <div className="App">
        <Dashboard />
        <div className="container mt-4">
          <Switch>
            <Route path={"/tp1"} component={Calculate} exact/>
            <Route path={"/tp2"} component={Articles} exact/>
            <Route path={"/tp3"} component={Repos} exact/>       
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
