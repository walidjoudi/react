import React from 'react'
import ReactPaginate from 'react-paginate'

const Pagine = ({ modifierPageCourante, currentPage }) => {
    return (
        <ReactPaginate
            pageCount={35}
            pageRangeDisplayed={4}
            marginPagesDisplayed={4}
            forcePage={currentPage-1}
            previousLabel={"Precedent"}
            nextLabel={"Suivant"}
            breakLabel={"..."}
            breakClassName={"page-item"}
            onPageChange={modifierPageCourante}
            containerClassName={"pagination"}
            pageClassName={"page-item"}
            previousLinkClassName={"page-link"}
            previousClassName={"page-item"}
            nextClassName={"page-item"}
            pageLinkClassName={"page-link"}
            breakClassName={"page-item"}
            breakLinkClassName={"page-link"}
            nextLinkClassName={"page-link"}
            activeClassName={"active"}
    />
    )
}

export default Pagine
