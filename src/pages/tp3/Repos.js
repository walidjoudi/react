import React, {useState, useEffect} from 'react'
import API from './axios'
import { Badge, Card, CardBody, CardImg, CardSubtitle, Spinner } from 'reactstrap'
import Pagine from './Pagine'

const Repos = () => {
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [currentPage, setCurrentPage] = useState(1)
    useEffect(() => {
        const fetchData = async () => {
            setLoading(true)
            let response = await API.get(`search/repositories?q=created:>2020-11-07&sort=stars
            &order=desc&page=${currentPage}`)
            setData(response.data.items)
            setLoading(false)
        }
        fetchData()
    }, [currentPage])

    const modifierPageCourante = (e) => {
        console.log(e.selected)
        setCurrentPage(e.selected + 1)
    }

    return (
        <div>
            <Pagine modifierPageCourante={modifierPageCourante} currentPage={currentPage} />
            {loading ? <Spinner color="danger" /> :
            data.map(d => {
                return(
                    <Card key={d.id}>
                        <CardBody>
                            <CardImg className={"mr-5"} style={{height: "180px",width: "180px"}}  width="180px" src={d.owner.avatar_url} />
                                <div className={"d-inline-block"}>
                                    <CardSubtitle ><b>{d.name}</b></CardSubtitle>
                                    <CardSubtitle >{d.description}</CardSubtitle>
                                    <Badge color="warning" className={"mr-2"}>Stars: {d.stargazers_count}</Badge>
                                    <Badge color="info" >Open issues: {d.open_issues_count}</Badge>   
                                </div>                     
                        </CardBody>
                    </Card>
                )
            })}
            <Pagine modifierPageCourante={modifierPageCourante} currentPage={currentPage} />
        </div>
    )
}

export default Repos
