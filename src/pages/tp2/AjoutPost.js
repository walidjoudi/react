import React, {useState} from 'react'
import { Button, Form, FormGroup, Input, Card, CardBody } from 'reactstrap';

const AjoutPost = (props) => {
    const [post, setPost] = useState({
        id: 0,
        user: '',
        content: '',
        likes: 0,
        dislikes: 0,
        comments: [
        ]
    })

    const handleChange = (e) => {
        setPost({...post, [e.target.name] : e.target.value})
    }
    const handleClick= () => {
        props.ajouterPost(post)
        setPost({
            id: 0,
            user: '',
            content: '',
            likes: 0,
            dislikes: 0,
            comments: [
            ]
        })
    }
    return (
        <Card>
            <CardBody>
                <Form>
                    <FormGroup>
                        <Input type="text" name="user" placeholder="Utilisateur" onChange={handleChange} value={post.user} />
                        <Input type="textarea" name="content" onChange={handleChange} value={post.content}/>
                    </FormGroup>
                    <Button onClick={handleClick} className={"btn btn-info"}>Ajouter ce poste</Button>
                </Form>
            </CardBody>
        </Card>
    )
}

export default AjoutPost
